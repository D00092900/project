import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFire, FirebaseListObservable} from 'angularfire2';
import { MainMenuPage } from "../main-menu/main-menu";
@Component({
  selector: 'page-mood-tracker',
  templateUrl: 'mood-tracker.html'
})
export class MoodTrackerPage {
  emotions: FirebaseListObservable<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, angFire: AngularFire) {
    this.emotions = angFire.database.list('/Emotions');
  }
  addEmotion(){
    this.navCtrl.push(MainMenuPage);
  }

}

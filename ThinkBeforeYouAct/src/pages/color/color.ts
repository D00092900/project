import { Component, ViewChild } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import {ToolMenuPage} from "../tool-menu/tool-menu";

@Component({
  selector: 'page-color',
  templateUrl: 'color.html'
})
export class ColorPage {
  signature = '';
  isDrawing = false;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 400,
    /*'backgroundColor': '#f6fbff',*/
    'background-image': 'url(../assets/images/colouring_Pic1.jpg)',
    'penColor': '#666a73'
  };

  constructor(public navController: NavController, public storage: Storage, public toastCtrl: ToastController) {}

  ionViewDidEnter() {
    this.signaturePad.clear()
    this.storage.get('savedSignature').then((data) => {
      this.signature = data;
    });
  }

  drawComplete() {
    this.isDrawing = false;
  }

  drawStart() {
    this.isDrawing = true;
  }

  savePad() {
    this.signature = this.signaturePad.toDataURL();
    this.storage.set('savedSignature', this.signature);
    this.signaturePad.clear();
    let toast = this.toastCtrl.create({
      message: 'Picture Saved.',
      duration: 3000
    });
    toast.present();
    this.navController.push(ToolMenuPage);
  }

  clearPad() {
    this.signaturePad.clear();
  }
}

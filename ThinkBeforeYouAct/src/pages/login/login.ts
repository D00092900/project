import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import {MoodTrackerPage} from "../mood-tracker/mood-tracker";
import {AuthProvider} from "../../providers/auth-provider";
import { SignupPage } from '../signup/signup';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginForm:FormGroup;
  email:any;
  password:any;

  /*  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  login() {
        this.navCtrl.push(MoodTrackerPage);
  }

  logout(): void {
    this.navCtrl.push(MoodTrackerPage);
  }
   */

   constructor(private navCtrl: NavController, private fb: FormBuilder, private auth: AuthProvider) {
     this.loginForm = this.fb.group({
       'email': ['', Validators.compose([Validators.required, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)])],
       'password': ['', Validators.compose([Validators.required, Validators.minLength(1)])]
     });

   /*  this.email = this.loginForm.controls['email'];
     this.password = this.loginForm.controls['password'];*/
   }

  loadSignUp() {
    this.navCtrl.push(SignupPage);
  }

     login()
     {this.navCtrl.setRoot(MoodTrackerPage);
       if (this.loginForm.valid) {
         var credentials = ({email: this.email.value, password: this.password.value});
         this.auth.loginWithEmail(credentials).subscribe(data => {

           console.log(data);

         }, error => {
           console.log(error);
           if (error.code == 'auth/user-not-found') {
             alert('User not found');
           }
         });
       }
     }

   }

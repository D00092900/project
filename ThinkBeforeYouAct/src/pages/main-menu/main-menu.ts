import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {DiaryPage} from "../diary/diary";
import {ToolMenuPage} from "../tool-menu/tool-menu";
import {SelfCarePage} from "../self-care/self-care";

@Component({
  selector: 'page-main-menu',
  templateUrl: 'main-menu.html'
})
export class MainMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}
  diary() {
    this.navCtrl.push(DiaryPage);
  }

  tools() {
    this.navCtrl.push(ToolMenuPage);
  }

  selfCare() {
    this.navCtrl.push(SelfCarePage);
  }
}

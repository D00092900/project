import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {ColorPage} from "../color/color";
import {IndoorActPage} from "../indoor-act/indoor-act";
import {OutdoorActPage} from "../outdoor-act/outdoor-act";
import {StrategiesPage} from "../strategies/strategies";

@Component({
  selector: 'page-tool-menu',
  templateUrl: 'tool-menu.html'
})
export class ToolMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}
  color() {
    this.navCtrl.push(ColorPage);
  }
  indoorAct(){
    this.navCtrl.push(IndoorActPage)
  }
  outdoorAct(){
    this.navCtrl.push(OutdoorActPage)
  }
  strategies(){
    this.navCtrl.push(StrategiesPage)
  }
}

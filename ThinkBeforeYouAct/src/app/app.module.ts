import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AngularFireModule } from 'angularfire2';
import { AuthProvider } from '../providers/auth-provider';
import { SignupPage} from '../pages/signup/signup';
import { MainMenuPage} from '../pages/main-menu/main-menu';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { MoodTrackerPage } from '../pages/mood-tracker/mood-tracker';
import { LoginPage } from '../pages/login/login';
import { DiaryPage } from '../pages/diary/diary';
import { SignaturePadModule } from 'angular2-signaturepad';
import { ColorPage } from "../pages/color/color";
import { ToolMenuPage } from "../pages/tool-menu/tool-menu";

import { Storage } from '@ionic/storage';
import {IndoorActPage} from "../pages/indoor-act/indoor-act";
import {OutdoorActPage} from "../pages/outdoor-act/outdoor-act";
import {StrategiesPage} from "../pages/strategies/strategies";
import {SupportServicesPage} from "../pages/support-services/support-services";
import {SelfCarePage} from "../pages/self-care/self-care";

export const firebaseConfig = {
  apiKey: "AIzaSyBQY27Ezpkl3FyefpnU-7yhJwrV2IY7Nxo",
  authDomain: "thinkbeforeyouact-9a5e0.firebaseapp.com",
  databaseURL: "https://thinkbeforeyouact-9a5e0.firebaseio.com",
  storageBucket: "thinkbeforeyouact-9a5e0.appspot.com",
  messagingSenderId: "1041674394455"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
      ResetPasswordPage,
      MainMenuPage,
    MoodTrackerPage,
      LoginPage,
      DiaryPage,
      ColorPage,
      ToolMenuPage,
      SignupPage,
    IndoorActPage,
    OutdoorActPage,
    StrategiesPage,
    SupportServicesPage,
    SelfCarePage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
      AngularFireModule.initializeApp(firebaseConfig),
    SignaturePadModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
      ResetPasswordPage,
      MainMenuPage,
    MoodTrackerPage,
      LoginPage,
      DiaryPage,
      ColorPage,
      ToolMenuPage,
      SignupPage,
    IndoorActPage,
    OutdoorActPage,
    StrategiesPage,
    SupportServicesPage,
    SelfCarePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Storage, AuthProvider]
})
export class AppModule {}

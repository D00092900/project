import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AuthProvider } from '../providers/auth-provider';
import { HomePage } from '../pages/home/home';
import {MainMenuPage} from "../pages/main-menu/main-menu";
import {LoginPage} from "../pages/login/login";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  constructor(platform: Platform, public auth: AuthProvider) {
    platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();

      /*auth.onAuth.subscribe((authState)=>{
        if (authState){
          console.log('Logged in user :', authState.auth.email);
        }
      });*/
    });
  }
}
